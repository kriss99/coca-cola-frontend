# CODE REDEMPTION APP FRONTEND

## Description

Final Project Assignment for 'Telerik Academy Alpha' with JavaScript: Design and implement a web application that will allow cashiers to scan codes, register redemption attempts and allow admins to mange them and view reports.

### Stack

Front-end:
- SPA - Angular
Git repository: https://gitlab.com/kriss99/coca-cola-frontend

Back-end:
- Database - MySQL
- REST API - NestJS
Git repository: https://gitlab.com/kriss99/coca-cola-backend

----------

# Getting started

## Installation

Run
    `git clone https://gitlab.com/kriss99/coca-cola-frontend`

Get into the project's folder
    `cd coca-cola-frontend/Prize-Redemption`

## In order to run the project
1. run `npm install` to install all dependencies
2. run `ng s -o` to open the project in a new tab in your browser
*Make sure that you have the back-end and database running too.

## Scripts

- `ng s` or `ng s -o` - Starts the application in your browser
- `ng test` - Runs the unit tests

# Functionality overview

## General functionality

- Authenticate users - login
- Normal users can scan codes
- Normal users can add redemption records
- Normal users can view their and their outlet's redemption records
- Admin users can CRUD normal users
- Admin users can create redemption record reports
- Admin users can filter and view all other users

## General page breakdown

- Sign in page (URL: /#/login) 
    - Uses JWT (store the token in localStorage)
<br />
<a href="./screenshots/login.jpg" target="_blank">
  <img src="./screenshots/login.jpg" width="280" alt="Image" />
</a>
<br />

- Profile page (URL: /#/profile/:userId)
    - List of user properties
    - Redemption history of the user
    - Redemption history of the user's outlet
    - Edit user form
    - Change user outlet form
<br />
<a href="./screenshots/profile.jpg" target="_blank">
  <img src="./screenshots/profile.jpg" width="280" alt="Image" />
</a>
<br />

- Scan code page (URL: /#/scan)
    - Camera window to scan codes
    - manual input field
    - submit button
<br />
<a href="./screenshots/scan-code.jpg" target="_blank">
  <img src="./screenshots/scan-code.jpg" width="280" alt="Image" />
</a>
<br />

- Report page (URL: /#/report)
    - Form with different filtering options (by date, user, outlet..)
    - Report results table with sorting options
    - Dynamic dashboard based on report results 
<br />
<a href="./screenshots/report.jpg" target="_blank">
  <img src="./screenshots/report.jpg" width="280" alt="Image" />
</a>
<br />

- Report page (URL: /#/create)
    - Forms for creating users, customers and outlets
    - Delete form for customer and outlet
<br />
<a href="./screenshots/create.jpg" target="_blank">
  <img src="./screenshots/create.jpg" width="280" alt="Image" />
</a>
<br />

- Search users page (URL: /#/search)
    - Filter to search all users
    - table with all users that fit search
<br />
<a href="./screenshots/search.jpg" target="_blank">
  <img src="./screenshots/search.jpg" width="280" alt="Image" />
</a>
<br />

- Sidebar (availabe at each page)
    - Buttons for each page
<br />
<a href="./screenshots/sidebar.jpg" target="_blank">
  <img src="./screenshots/sidebar.jpg" width="280" alt="Image" />
</a>
<br />

- Top bar (availabe at each page)
     - Login/Logout button
     - Change theme button
     - Contact help desk button
     - Show sidebar button
<br />

----------

# Authors

Kriss Svilenov

Radoslav Donchev