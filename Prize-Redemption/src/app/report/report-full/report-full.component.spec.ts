import { TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReportService } from '../../core/services/report/report.service';
import { SharedModule } from '../../shared/shared.module';
import { ReportFullComponent } from './report-full.component';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { ActivatedRoute } from '@angular/router';
import { ReportResultsComponent } from '../report-results/report-results.component';
import { ReportFormComponent } from '../report-form/report-form.component';
import { ReportDashboardsComponent } from '../report-dashboards/report-dashboards.component';

describe('ReportFullComponent', () => {
  let fixture;
  const reportService = jasmine.createSpyObj('ReportService', ['getReportResults']);
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const userService = jasmine.createSpyObj('userService', ['getUserIdByUsername', 'getUserIdByEmail']);
  const route = jasmine.createSpyObj('ActivatedRoute', ['data']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReportFullComponent,
        ReportResultsComponent,
        ReportFormComponent,
        ReportDashboardsComponent,
      ],
      imports: [
        SharedModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: route,
        },
        {
          provide: UserService,
          useValue: userService,
        },
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: ReportService,
          useValue: reportService,
        },
        ReportFullComponent,
        {provide: APP_BASE_HREF, useValue : '/' }
      ]
    });
  }));

  afterEach(() => {
    if (fixture) {
      if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
        (fixture.nativeElement as HTMLElement).remove();
      }
    }
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(ReportFullComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  // it('should initialize and update outlets and customers variables', async () => {
  //   route.data.and.returnValue(of({
  //     outlets: 'test1',
  //     customers: 'test2',
  //   }));
  //   fixture = TestBed.createComponent(ReportFullComponent);
  //   const component = fixture.debugElement.componentInstance;

  //   await fixture.detectChanges();
  //   route.data.subscribe(
  //     (res) => {
  //       expect(component.outlets).toBe('test1');
  //       expect(component.customers).toBe('test2');
  //     }
  //   );
  // });

  describe('getReport', () => {
    it('should call userService.getUserIdByUsername if userPropertyType = username', () => {
      const component: ReportFullComponent = TestBed.get(ReportFullComponent);
      const reportRequestData: any = { userPropertyType: 'username', userPropertyValue: 'test'};

      userService.getUserIdByUsername.calls.reset();
      component.getReport(reportRequestData);
      expect(userService.getUserIdByUsername).toHaveBeenCalledTimes(1);
      expect(userService.getUserIdByUsername).toHaveBeenCalledWith('test');
    });

    it('should call userService.getUserIdByEmail if userPropertyType = email', () => {
      const component: ReportFullComponent = TestBed.get(ReportFullComponent);
      const reportRequestData: any = { userPropertyType: 'email', userPropertyValue: 'test1'};

      userService.getUserIdByEmail.calls.reset();
      component.getReport(reportRequestData);
      expect(userService.getUserIdByEmail).toHaveBeenCalledTimes(1);
      expect(userService.getUserIdByEmail).toHaveBeenCalledWith('test1');
    });

    it('should not call getUserIdByUsername or getUserIdByEmail if userPropertyType != username or email', () => {
      const component: ReportFullComponent = TestBed.get(ReportFullComponent);
      const reportRequestData: any = { userPropertyType: 'test2', userPropertyValue: 'test3'};

      userService.getUserIdByEmail.calls.reset();
      userService.getUserIdByUsername.calls.reset();
      component.getReport(reportRequestData);
      expect(userService.getUserIdByUsername).toHaveBeenCalledTimes(0);
      expect(userService.getUserIdByEmail).toHaveBeenCalledTimes(0);
    });

  });

  describe('sendReportRequest', () => {
    it('should throw if reportRequestData = null or undefined', () => {
      const component: ReportFullComponent = TestBed.get(ReportFullComponent);
      const reportRequestData: any = null;

      expect( () => { component.sendReportRequest(reportRequestData, 'test4'); } )
      .toThrow(new Error('reportRequestData is undefined or null!'));
    });

    it('should call reportService.getReportResults with correct values', () => {
      const component: ReportFullComponent = TestBed.get(ReportFullComponent);
      reportService.getReportResults.and.returnValue(of(''));
      const reportRequestData: any = {
        periodCheckbox: false,
        fromDate: undefined,
        untilDate: undefined,
        customerId: 'test2',
        outletId: 'test3',
      };

      reportService.getReportResults.calls.reset();
      component.sendReportRequest(reportRequestData, 'test4');
      expect(reportService.getReportResults).toHaveBeenCalledTimes(1);
      expect(reportService.getReportResults).toHaveBeenCalledWith(
        undefined, undefined, 'test4', 'test2', 'test3');
    });

  });

});
