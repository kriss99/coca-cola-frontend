import { TestBed, async } from '@angular/core/testing';
import { of } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ReportDashboardsComponent } from './report-dashboards.component';
import { ReportService } from '../../core/services/report/report.service';
import { SharedModule } from '../../shared/shared.module';

describe('ReportDashboardsComponent', () => {
  let fixture;
  const reportService = jasmine.createSpyObj('ReportService', ['report$']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReportDashboardsComponent,
      ],
      imports: [
        SharedModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: ReportService,
          useValue: reportService,
        },
        ReportDashboardsComponent,
        {provide: APP_BASE_HREF, useValue : '/' }
      ]
    });
  }));

  afterEach(() => {
    if (fixture) {
      if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
        (fixture.nativeElement as HTMLElement).remove();
      }
    }
  });

  it('should create the component', () => {
    fixture = TestBed.createComponent(ReportDashboardsComponent);
    const component = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('if correct results array passed it should initialize and update data variables', async () => {
    reportService.report$ = of('');
    fixture = TestBed.createComponent(ReportDashboardsComponent);
    const component = fixture.debugElement.componentInstance;
    const testRecord1 = {
      id: 'testId1',
      timeStamp: 'October 13, 2014 11:13:00',
      status: 'Redeemed',
    };
    const testRecord2 = {
      id: 'testId2',
      timeStamp: 'October 14, 2014 22:23:00',
      status: 'Cancelled',
    };
    component.records = [testRecord1, testRecord2];

    await fixture.detectChanges();
    reportService.report$.subscribe(
      (res) => {
        expect(component.data1.labels).toEqual([ '13/10/2014', '14/10/2014' ]);
        expect(component.data1.datasets[0].data).toEqual([1, 1]);
      }
    );
  });

  describe('changeDateFormat', () => {
        it('should return properly updated date', () => {
        const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);
        const dateObj: Date = new Date('October 13, 2014 11:13:00');

        const updatedDate = component.changeDateFormat(dateObj);
        expect(updatedDate).toBe('13/10/2014');
      });

    });

  describe('getArrayDaysBetweenFirstAndLastDates', () => {

          it('should return correct new array', () => {
            const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);
            const testRecord1 = {
              id: 'testId1',
              timeStamp: 'October 13, 2014 11:13:00',
              status: 'testStatus1',
            };
            const testRecord2 = {
              id: 'testId2',
              timeStamp: 'October 16, 2014 10:13:00',
              status: 'testStatus2',
            };
            const records: any = [testRecord1, testRecord2];

            const datesArray = component.getArrayDaysBetweenFirstAndLastDates(records);
            expect(datesArray).toEqual([ '13/10/2014', '14/10/2014', '15/10/2014', '16/10/2014' ]);
          });

          it('should return array with only one element if all records have the same date', () => {
            const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);
            const testRecord1 = {
              id: 'testId1',
              timeStamp: 'October 13, 2014 11:13:00',
              status: 'testStatus1',
            };
            const testRecord2 = {
              id: 'testId2',
              timeStamp: 'October 13, 2014 22:23:00',
              status: 'testStatus2',
            };
            const records: any = [testRecord1, testRecord2];

            const datesArray = component.getArrayDaysBetweenFirstAndLastDates(records);
            expect(datesArray).toEqual([ '13/10/2014' ]);
          });

          it('should return null if no records are passed', () => {
            const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);
            const records: any = [];

            const datesArray = component.getArrayDaysBetweenFirstAndLastDates(records);
            expect(datesArray).toEqual(null);
          });

    });

  describe('getRecordNumbersByDates', () => {
    it('should fill empty days with zeroes and return correct array', () => {
      const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);

      const testRecord1 = {
        id: 'testId1',
        timeStamp: 'October 13, 2014 11:13:00',
        status: 'Redeemed',
      };
      const testRecord2 = {
        id: 'testId2',
        timeStamp: 'October 13, 2014 22:23:00',
        status: 'Cancelled',
      };
      const records: any = [testRecord1, testRecord2];

      const days = [ '13/10/2014', '14/10/2014', '15/10/2014', '16/10/2014' ];

      const recordNumbersArray = component.getRecordNumbersByDates(records, days);
      expect(recordNumbersArray).toEqual([ 2, 0, 0, 0 ]);
    });

    it('should filter records based on passed status and return correct array', () => {
      const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);

      const testRecord1 = {
        id: 'testId1',
        timeStamp: 'October 14, 2014 11:13:00',
        status: 'Redeemed',
      };
      const testRecord2 = {
        id: 'testId2',
        timeStamp: 'October 14, 2014 22:23:00',
        status: 'Cancelled',
      };
      const records: any = [testRecord1, testRecord2];

      const days = [ '13/10/2014', '14/10/2014', '15/10/2014' ];

      const recordNumbersArray = component.getRecordNumbersByDates(records, days, 'Cancelled');
      expect(recordNumbersArray).toEqual([ 0, 1, 0 ]);
    });

    it('should return null if no records are passed', () => {
      const component: ReportDashboardsComponent = TestBed.get(ReportDashboardsComponent);
      const records: any = [];

      const days = [ '13/10/2014', '14/10/2014', '15/10/2014' ];

      const recordNumbersArray = component.getRecordNumbersByDates(records, days);
      expect(recordNumbersArray).toEqual(null);
    });

  });

});
