import { Component, OnInit, ViewChild, Input, OnDestroy } from '@angular/core';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';
import { MatPaginator, MatTableDataSource, MatSort, Sort } from '@angular/material';
import { Subscription } from 'rxjs';
import { ReportService } from 'src/app/core/services/report/report.service';

@Component({
  selector: 'app-report-results',
  templateUrl: './report-results.component.html',
  styleUrls: ['./report-results.component.css']
})
export class ReportResultsComponent implements OnInit, OnDestroy {
  columnsToDisplay = ['timeStamp', 'redemptionCode', 'status', 'user', 'outlet'];

  public dataSource: MatTableDataSource<RedemptionRecord>;

  @Input()
  public records: RedemptionRecord[];
  private subscription: Subscription;
  public showTable = false;

  @ViewChild(MatSort, {static: false}) sort: MatSort;
  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(private readonly reportService: ReportService) {
      this.dataSource = new MatTableDataSource<RedemptionRecord>(this.records);
      setTimeout(() => this.dataSource.sort = this.sort);
      setTimeout(() => this.dataSource.paginator = this.paginator);
    }

  ngOnInit() {

    this.subscription = this.reportService.report$.subscribe(res => {
      if (this.records.length === 0) {
        this.showTable = false;
      } else {
        this.showTable = true;
        this.records = this.records.sort((a, b) => {
          return new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime();
        });
        this.dataSource = new MatTableDataSource<RedemptionRecord>(this.records);
        this.dataSource.paginator = this.paginator;
        setTimeout(() => this.dataSource.sort = this.sort);
      }

    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
