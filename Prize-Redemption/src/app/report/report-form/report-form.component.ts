import { Component, OnInit, Output, EventEmitter, Input, OnDestroy } from '@angular/core';
import { ReportService } from 'src/app/core/services/report/report.service';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { Customer } from 'src/app/common/interfaces/customer';
import { Subscription } from 'rxjs';
import { AppService } from 'src/app/core/services/app/app.service';

@Component({
  selector: 'app-report-form',
  templateUrl: './report-form.component.html',
  styleUrls: ['./report-form.component.css']
})
export class ReportFormComponent implements OnInit, OnDestroy {

  @Output()
  public reportRequest = new EventEmitter<{formData: any}>();

  @Input()
  public outlets: Outlet[];

  @Input()
  public customers: Customer[];

  private subscription: Subscription;
  public currentTheme = 'default';

  constructor(
    public service: ReportService,
    private readonly appService: AppService,
    ) { }

  userProperties = ['id', 'username', 'email'];

  ngOnInit() {
    this.subscription = this.appService.theme$.subscribe(
      res => {
        this.currentTheme = res;
      });
  }


  onClear() {
    this.service.form.reset();
    this.service.initializeFormGroup();
  }

  onSubmit() {
    if (this.service.form.valid) {
      this.reportRequest.emit(this.service.form.value);
    }
  }

  public getThemePrimaryClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: true, defaultAccent: false };
    } else {
      return { altPrimary: true, altAccent: false };
    }
  }

  public getThemeAccentClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: false, defaultAccent: true };
    } else {
      return { altPrimary: false, altAccent: true };
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
    this.service.form.reset();
  }

}
