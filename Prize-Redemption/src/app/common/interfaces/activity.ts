export interface Activity {
    id: string;
    timeStamp: string;
    __outlet__: string;
    __redemptionCode__: string;
}
