export interface Customer {
    id: string;
    isDeleted: boolean;
    name: string;
}
