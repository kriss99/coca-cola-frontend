export interface HelpDesk {
    subject: string;
    description: string;
}
