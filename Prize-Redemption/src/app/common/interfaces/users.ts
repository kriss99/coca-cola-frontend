export interface Users {

    username: string;

    email: string;

    __outlet__: string;
}
