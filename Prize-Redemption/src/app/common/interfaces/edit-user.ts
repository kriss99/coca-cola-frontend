export interface EditUser {
    email?: string;
    username?: string;
    password?: string;
}
