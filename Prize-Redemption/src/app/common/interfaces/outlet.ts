export interface Outlet {
    id: string;

    name: string;

    customerId: string;

    customerName: string;
}
