import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EditUser } from 'src/app/common/interfaces/edit-user';
import { CreateUser } from 'src/app/common/interfaces/create-user';
import { HelpDesk } from 'src/app/common/interfaces/help-desk';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UserService {
  constructor(private readonly http: HttpClient) { }

  public sendEmailToHelpDesk(textToSend: HelpDesk) {
    return this.http.post('http://localhost:3000/user/help-desk', textToSend);
  }
  public getUserActivity(id: string) {
    return this.http.get(`http://localhost:3000/user/redemptionRecords/${id}`);
  }

  public createUser(user: CreateUser) {
    return this.http.post('http://localhost:3000/user/create', user);
  }

  public updateUser(user: EditUser, id: string) {
    return this.http.put(`http://localhost:3000/user/${id}`, user);
  }
  public getUserByID(id: string) {
    return this.http.get(`http://localhost:3000/user/${id}`);
  }
  public deleteUser(id: string) {
    return this.http.delete(`http://localhost:3000/user/${id}`);
  }

  public updateUserOutlet(outletToChange, userId: string) {
    return this.http.put(`http://localhost:3000/user/${userId}/outlet`, outletToChange);
  }

  public getAllUsers() {
    return this.http.get('http://localhost:3000/user');
  }

  public getUserIdByUsername(username: string) {
    return this.http.get(`http://localhost:3000/user/username/${username}`);
  }

  public getUserIdByEmail(email: string): Observable<{}> {
    return this.http.get(`http://localhost:3000/user/email/${email}`);
  }

  public getOutletActivity(id: string) {
    return this.http.get(`http://localhost:3000/user/outlet-activity/${id}`);
  }

  public getAllEmails() {
    return this.http.get('http://localhost:3000/user/all-emails');
  }

  public getAllUsernames() {
    return this.http.get('http://localhost:3000/user/all-usernames');
  }
}
