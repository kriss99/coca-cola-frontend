import { StorageService } from '../storage/storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ScanCodeService {



  constructor(
    private readonly http: HttpClient,
    private readonly storage: StorageService,
  ) { }

  public checkCode(redemptionCode: string): Observable<{redemptionCode}> {
    return this.http.post<{redemptionCode}>('http://localhost:3000/user/check-code', {
      redemptionCode
    });
  }

  public markCode(redemptionCode: string, status: string): Observable<{}> {
    return this.http.post<{redemptionCode: string, status: string}>('http://localhost:3000/user/mark-code', {
      redemptionCode, status
    });
  }

  public addPrizeItemToCode(redemptionCode: string, itemPrizeCode: string): Observable<{}> {
    return this.http.post<{redemptionCode: string, itemPrizeCode: string}>('http://localhost:3000/user/add-prize-to-code', {
      redemptionCode, itemPrizeCode
    });
  }

  public reportCode(redemptionCode: string): Observable<{}> {
    return this.http.post<{redemptionCode: string}>('http://localhost:3000/user/report-code', {
      redemptionCode
    });
  }

}
