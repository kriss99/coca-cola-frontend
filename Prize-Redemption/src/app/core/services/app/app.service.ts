import { Injectable } from '@angular/core';
import { FormGroup, FormControl, Validators, ValidatorFn } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  private readonly themeSubject$ = new BehaviorSubject<string>('default');

  public get theme$() {
    return this.themeSubject$.asObservable();
  }

  constructor( ) {
  }

  public changeThemeSubject() {
      if (this.themeSubject$.value === 'default') {
        this.themeSubject$.next('alternate');
      } else {
        this.themeSubject$.next('default');
      }
  }

}
