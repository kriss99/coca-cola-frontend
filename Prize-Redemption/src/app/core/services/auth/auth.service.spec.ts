import { TestBed } from '@angular/core/testing';

import { AuthService } from './auth.service';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../storage/storage.service';
import { of } from 'rxjs';

describe('AuthService', () => {
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post']);
  const storage = jasmine.createSpyObj('StorageService', ['get', 'set', 'remove']);

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      {
        provide: HttpClient,
        useValue: http,
      },
      {
        provide: StorageService,
        useValue: storage,
      }

    ]
  }));

  it('should be created', () => {
    // Arrange
    const service: AuthService = TestBed.get(AuthService);

    // Act & Assert
    expect(service).toBeTruthy();
  });

  it('login should log the user in', () => {

    http.post.and.returnValue(of({
      token: 'token',
      user: {
        name: 'test',
      },
    }));

    const service: AuthService = TestBed.get(AuthService);

    service.login('email', 'password').subscribe(
      (res) => {
        expect(res.user.name).toBe('test');
      }
    );

  });

  // it('login should call auth.post', () => {
  //   const service: AuthService = TestBed.get(AuthService);

  //   http.post.calls.reset();
  //   service.login('email', 'password').subscribe(
  //     () => expect(http.post).toHaveBeenCalledTimes(1)
  //   );

  // });

  // it('login should update the subject', () => {

  //   http.post.and.returnValue(of({
  //     token: 'token',
  //     username: 'test2',
  //   }));

  //   const service: AuthService = TestBed.get(AuthService);

  //   service.login('email', 'password').subscribe(
  //     () => {
  //       service.user$.subscribe(
  //         (username) => expect(username).toBe('test2')
  //       );
  //     }
  //   );

  // });

  it(`logout should change the subject to <null>`, () => {
    const service: AuthService = TestBed.get(AuthService);

    service.logout();

    service.user$.subscribe((username) => expect(username).toBe(null));

  });

  it(`logout should call storage.remove 6 times`, () => {
    const service: AuthService = TestBed.get(AuthService);
    storage.remove.calls.reset();

    service.logout();

    expect(storage.remove).toHaveBeenCalledTimes(6);
    expect(storage.remove).toHaveBeenCalledWith('token');
    expect(storage.remove).toHaveBeenCalledWith('username');

  });

});
