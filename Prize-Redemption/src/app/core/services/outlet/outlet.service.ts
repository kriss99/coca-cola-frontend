import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class OutletService {

  constructor(private readonly http: HttpClient) { }

  public getAllOutlets() {
    return this.http.get('http://localhost:3000/outlet');
  }

  public createOutlet(outlet) {
    return this.http.post('http://localhost:3000/outlet', outlet);
  }

  public deleteOutlet(outletID) {
    return this.http.delete(`http://localhost:3000/outlet/${outletID}`);
  }
}
