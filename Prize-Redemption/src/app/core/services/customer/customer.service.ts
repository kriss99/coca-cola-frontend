import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomerService {

  constructor(private readonly http: HttpClient) { }

  public getCustomers() {
    return this.http.get('http://localhost:3000/customer');
  }

  public createCustomer(customer) {
    return this.http.post('http://localhost:3000/customer', customer);
  }

  public deleteCustomer(customerID) {
    return this.http.delete(`http://localhost:3000/customer/${customerID}`);
  }
}
