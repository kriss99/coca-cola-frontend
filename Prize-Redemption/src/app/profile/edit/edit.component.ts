import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { StorageService } from 'src/app/core/services/storage/storage.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  @Output()
  public editUser = new EventEmitter();
  public editGroup: FormGroup;

  constructor(
    private readonly formBuilder: FormBuilder,
    private readonly storage: StorageService) { }

  ngOnInit() {
    this.editGroup = this.formBuilder.group({
      email: [
        '',
        [
          Validators.minLength(5),
          Validators.pattern(
            // tslint:disable-next-line: max-line-length
            /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
          )
        ]
      ],
      username: ['', [Validators.minLength(4), Validators.maxLength(20)]],
      password: ['', [
        Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)
      ],
      ],
    });
  }

  public edit(): void {
    const user = this.editGroup.value;
    const emitUser = {};

    if (user.username !== '') {
      // tslint:disable-next-line: no-string-literal
      emitUser['username'] = user.username;
    }
    if (user.password !== '') {
      // tslint:disable-next-line: no-string-literal
      emitUser['password'] = user.password;
    }
    this.editUser.emit(emitUser);
  }
}
