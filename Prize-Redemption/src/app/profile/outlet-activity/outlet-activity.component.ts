import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { MatPaginator, MatTableDataSource } from '@angular/material';
import { OutletActivity } from 'src/app/common/interfaces/outlet-activity';

@Component({
  selector: 'app-outlet-activity',
  templateUrl: './outlet-activity.component.html',
  styleUrls: ['./outlet-activity.component.css']
})
export class OutletActivityComponent implements OnInit {

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @Input()
  public outletActivity: OutletActivity[];
  public displayedColumns: string[] = ['Username', 'Barcode', 'Status', 'Outlet'];
  public dataSource;

  ngOnInit() {
    this.dataSource = new MatTableDataSource<any>(this.outletActivity);
    this.dataSource.paginator = this.paginator;
  }

}
