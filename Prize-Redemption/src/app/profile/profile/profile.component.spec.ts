import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ProfileComponent } from './profile.component';
import { ActivityComponent } from '../activity/activity.component';
import { EditComponent } from '../edit/edit.component';
import { ChangeOutletComponent } from '../change-outlet/change-outlet.component';
import { OutletActivityComponent } from '../outlet-activity/outlet-activity.component';
import { ProfileRoutingModule } from '../profile-routing.module';
import { SharedModule } from '../../shared/shared.module';
import { ActivatedRoute, Data, Params, Router } from '@angular/router';
import { StorageService } from '../../core/services/storage/storage.service';
import { UserService } from '../../core/services/user/user.service';
import { NotificatorService } from '../../core/services/notificator/notificator.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

describe('ProfileComponent', () => {
  let component: ProfileComponent;
  let fixture: ComponentFixture<ProfileComponent>;
  const user = {
    username: 'Kriss',
    createdOn: '12/12/12',
    email: 'cucumber@gmail.com',
    outletName: 'bul aprilsko 15'
  };
  const activity = [{
    id: '123',
    timeStamp: '12/12/12',
    __outlet__: 'bul aprilsko',
    __redemptionCode: '123456'
  }];
  const allOutlets = [{
    id: '123',
    name: 'bul horizontal',
    customerId: '0858642075',
    customerName: 'Walmart',
  }];
  const outletActivity = [{
    __outlet__: 'bul horizontal',
    __redemptionCode__: '90875ok61',
    __user__: 'Tom',
    status: 'redeemed',
  }];

  const storage = jasmine.createSpyObj('StorageService', ['set', 'get', 'remove']);
  const userService = jasmine.createSpyObj(
    'UserService', ['updateUser', 'deleteUser', 'updateUserOutlet', 'getUserActivity', 'getOutletActivity']);
  const notificatorService = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const router = jasmine.createSpyObj('Router', ['navigate']);

  const activatedRoute = {
    data: {
      subscribe: (fn: (value: Data) => void) => fn({
        user,
        activity,
        allOutlets,
        outletActivity
      }),
    },
    params: {
      subscribe: (fn: (value: Params) => void) => fn({
        username: 'username',
      }),
    },
    snapshot: {
      params: {
        id: '10-10-10'
      }
    },
  };
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ProfileComponent,
        ActivityComponent,
        EditComponent,
        ChangeOutletComponent,
        OutletActivityComponent
      ],
      imports: [
        SharedModule,
        ProfileRoutingModule,
        BrowserAnimationsModule
      ],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: activatedRoute
        },
        {
          provide: StorageService,
          useValue: storage,
        },
        {
          provide: UserService,
          useValue: userService,
        },
        {
          provide: NotificatorService,
          useValue: notificatorService,
        },
        {
          provide: Router,
          useValue: router,
        },
      ]
    });
  }));

  afterEach(() => {
    if (fixture) {
      if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
        (fixture.nativeElement as HTMLElement).remove();
      }
    }
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;
    expect(component).toBeTruthy();
  });

  it('activity should contain information from the resolver', async () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    let activityTest = {};

    activatedRoute.data.subscribe(data => {
      activityTest = data.activity;
    });

    expect(activityTest).toContain({
      id: '123',
      timeStamp: '12/12/12',
      __outlet__: 'bul aprilsko',
      __redemptionCode: '123456'
    });
  });

  it('should match the user information from the resolver', async () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    let username: string;
    let createdOn: string;
    let email: string;
    let outletName: string;

    activatedRoute.data.subscribe(data => {
      username = data.user.username;
      createdOn = data.user.createdOn;
      email = data.user.email;
      outletName = data.user.outletName;
    });

    expect(username).toBe('Kriss');
    expect(createdOn).toBe('12/12/12');
    expect(email).toBe('cucumber@gmail.com');
    expect(outletName).toBe('bul aprilsko 15');
  });

  it('should match the outletActivity information from the resolver', async () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    let outletActivityTest = {};
    activatedRoute.data.subscribe(data => {
      outletActivityTest = data.outletActivity;
    });

    expect(outletActivityTest).toContain({
      __outlet__: 'bul horizontal',
      __redemptionCode__: '90875ok61',
      __user__: 'Tom',
      status: 'redeemed',
    });
  });

  it('should match the outlet information from the resolver', async () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    let outlets = {};
    activatedRoute.data.subscribe(data => {
      outlets = data.allOutlets;
    });

    expect(outlets).toContain({
      id: '123',
      name: 'bul horizontal',
      customerId: '0858642075',
      customerName: 'Walmart',
    });
  });

  it('should display id when calling', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;
    const id = activatedRoute.snapshot.params.id;

    expect(id).toBe('10-10-10');
  });

  it('storage should return Admin when called', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    storage.get.and.callFake(() => {
      return 'Admin';
    });

    expect(storage.get('role')).toBe('Admin');
  });

  it('should call editUser()', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    spyOn(component, 'editUser');
    component.editUser({ email: 'carrot@gmail.com' });
    expect(component.editUser).toHaveBeenCalled();
  });

  it('should call deleteUser()', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    spyOn(component, 'deleteUser');
    component.deleteUser();
    expect(component.deleteUser).toHaveBeenCalled();
  });

  it('should call updateUserOutlet()', () => {
    fixture = TestBed.createComponent(ProfileComponent);
    const app = fixture.debugElement.componentInstance;

    spyOn(component, 'updateUserOutlet');
    component.updateUserOutlet('1');
    expect(component.updateUserOutlet).toHaveBeenCalled();
  });
});
