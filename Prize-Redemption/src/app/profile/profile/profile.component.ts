import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { EditUser } from 'src/app/common/interfaces/edit-user';
import { UserService } from 'src/app/core/services/user/user.service';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { Activity } from 'src/app/common/interfaces/activity';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { OutletActivity } from 'src/app/common/interfaces/outlet-activity';
import { AppService } from 'src/app/core/services/app/app.service';
import { Subscription } from 'rxjs';
import { DialogService } from 'src/app/core/services/dialog/dialog.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private subscription: Subscription;
  public currentTheme = 'default';
  public username: string;
  public role: string;
  public createdOn: string;
  public email: string;
  public activity: Activity[];
  public outletName: string;
  public allOutlets: Outlet[];
  public id: string;
  public outletActivity: OutletActivity[];

  constructor(
    public storage: StorageService,
    private readonly route: ActivatedRoute,
    private readonly userService: UserService,
    private readonly notificator: NotificatorService,
    private readonly routed: Router,
    private readonly appService: AppService,
    private readonly confirmDialog: DialogService,
  ) { }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.activity = data.activity;
      this.username = data.user.username;
      this.createdOn = data.user.createdOn;
      this.email = data.user.email;
      this.outletName = data.user.outletName;
      this.allOutlets = data.outlet;
      this.outletActivity = data.outletActivity;
      this.id = this.route.snapshot.params.id;
      this.role = this.storage.get('role');
    });
    this.subscription = this.appService.theme$.subscribe(
      res => {
        this.currentTheme = res;
      });
  }

  public editUser(user: EditUser): void {
    this.userService
      .updateUser(user, this.id)
      .subscribe(
        () => {
          if (user.email) {
            this.email = user.email;
          }
          if (user.username) {
            this.username = user.username;
          }
          this.notificator.success('Successfully updated user!');
        },
        (err) => console.log(err)
      );
  }

  public deleteUser(): void {
    this.confirmDialog.openConfirmDialog(`Are you sure you would like to delete this user?`)
              .afterClosed().subscribe(selectedYes => {
                if (selectedYes === true) {
                  this.userService.deleteUser(this.id).subscribe(
                    () => {
                      this.notificator.success('User is now deleted!');
                      this.routed.navigate(['/home']);
                    },
                    err => {
                      this.notificator.error(err);
                      this.routed.navigate(['/home']);
                    }
                  );
                }
              });

  }

  public updateUserOutlet(outletInfo): void {
    this.outletName = outletInfo.nameOfMyOutlet;
    this.userService.updateUserOutlet({ outletId: outletInfo.idOfOutlet }, this.id).subscribe(
      () => {
        this.userService.getUserActivity(this.id).subscribe((data: Activity[]) => {
          this.activity = data;
        });
        this.userService.getOutletActivity(this.id).subscribe((data: OutletActivity[]) => {
          this.outletActivity = data;
        });
        this.notificator.success('Successfully changed your outlet!');
      },
      () => this.notificator.error('An error ocurred when tried to change your outlet!')
    );
  }

  public getThemePrimaryClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: true, defaultAccent: false };
    } else {
      return { altPrimary: true, altAccent: false };
    }
  }

  public getThemeAccentClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: false, defaultAccent: true };
    } else {
      return { altPrimary: false, altAccent: true };
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
