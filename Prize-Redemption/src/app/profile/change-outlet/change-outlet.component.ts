import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Outlet } from 'src/app/common/interfaces/outlet';

@Component({
  selector: 'app-change-outlet',
  templateUrl: './change-outlet.component.html',
  styleUrls: ['./change-outlet.component.css']
})
export class ChangeOutletComponent {

  @Output()
  public outletName = new EventEmitter();
  @Input()
  public myOutletName: string;
  @Input()
  public outlets: Outlet[];

  public saveOutlet(chosenOutlet: string): void {
    this.myOutletName = chosenOutlet;
    const idOfOutlet: string = this.outlets.find(x => x.name === chosenOutlet).id;
    this.outletName.emit({ idOfOutlet, nameOfMyOutlet: this.myOutletName });
  }
}
