// import { async, ComponentFixture, TestBed } from '@angular/core/testing';
// import { SearchComponent } from './search.component';
// import { UserService } from '../../core/services/user/user.service';
// import { SearchRoutingModule } from '../search-routing.module';
// import { SharedModule } from '../../shared/shared.module';

// describe('SearchComponent', () => {
//   let component: SearchComponent;
//   let fixture: ComponentFixture<SearchComponent>;
//   const userService = jasmine.createSpyObj(
//     'UserService', ['updateUser', 'deleteUser', 'updateUserOutlet', 'getUserActivity', 'getAllUsers']
//   );

//   beforeEach(async(() => {
//     TestBed.configureTestingModule({
//       declarations: [SearchComponent],
//       providers: [
//         {
//           provide: UserService,
//           useValue: userService
//         }
//       ],
//       imports: [
//         SearchRoutingModule,
//         SharedModule
//       ]
//     })
//       .compileComponents();
//   }));

//   beforeEach(() => {
//     fixture = TestBed.createComponent(SearchComponent);
//     component = fixture.componentInstance;
//     fixture.detectChanges();
//   });

//   it('should create', () => {
//     expect(component).toBeTruthy();
//   });
// });
