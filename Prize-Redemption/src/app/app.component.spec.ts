import { HomeComponent } from './components/home/home.component';
import { SharedModule } from './shared/shared.module';
import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { of } from 'rxjs';
import { APP_BASE_HREF } from '@angular/common';
import { NotificatorService } from './core/services/notificator/notificator.service';
import { AuthService } from './core/services/auth/auth.service';
import { HelpDeskComponent } from './components/help-desk/help-desk.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { SidebarComponent } from './components/sidebar/sidebar.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('AppComponent', () => {
  let fixture;
  const notificator = jasmine.createSpyObj('NotificatorService', ['success', 'error']);
  const auth = jasmine.createSpyObj('AuthService', ['login', 'logout', 'register']);
  const http = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put', 'delete']);
  const router = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        HomeComponent,
        LoginComponent,
        NavbarComponent,
        HelpDeskComponent,
        NotFoundComponent,
        SidebarComponent,
      ],
      imports: [
        SharedModule,
        AppRoutingModule,
        NgxSpinnerModule,
        BrowserAnimationsModule,
      ],
      providers: [
        {
          provide: NotificatorService,
          useValue: notificator,
        },
        {
          provide: HttpClient,
          useValue: http,
        },
        {
          provide: AuthService,
          useValue: auth,
        },
        AppComponent,
        {provide: APP_BASE_HREF, useValue : '/' }
      ],
      schemas: [ NO_ERRORS_SCHEMA ],
    });
  }));

  afterEach(() => {
    if (fixture) {
      if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
        (fixture.nativeElement as HTMLElement).remove();
      }
    }
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should initialize with the correct logged user data', async () => {
    auth.user$ = of('testName');
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    await fixture.detectChanges();
    expect(app.username).toBe('testName');
  });

  it('should initialize with empty username when the user is logged out', async () => {
    auth.user$ = of(null);
    fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;

    await fixture.detectChanges();
    expect(app.username).toBe('');
  });

  describe('logout', () => {
      it('should call auth.logout() once', () => {
        const component: AppComponent = TestBed.get(AppComponent);

        auth.logout.calls.reset();
        component.logout();
        expect(auth.logout).toHaveBeenCalledTimes(1);
      });

      it('should call notificator.success() once', () => {
        const component: AppComponent = TestBed.get(AppComponent);

        notificator.success.calls.reset();
        component.logout();
        expect(notificator.success).toHaveBeenCalledTimes(1);
      });

      // it('should call route.navigate() once', () => {
      //   const component: AppComponent = TestBed.get(AppComponent);

      //   router.navigate.calls.reset();
      //   component.logout();
      //   expect(router.navigate).toHaveBeenCalledTimes(1);
      // });

    });

  });
