import { Component, OnInit, OnDestroy } from '@angular/core';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth/auth.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit, OnDestroy {
  public myID: string;
  public role: string;
  public subscription: Subscription;

  constructor(
    private readonly storage: StorageService,
    private readonly authService: AuthService
  ) { }

  ngOnInit(): void {
    this.subscription = this.authService.user$.subscribe(data => {
      this.role = this.storage.get('role');
      const id = this.storage.get('id');
      if (id) {
        this.myID = id;
      }
    });
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

  public showAdminRoutes(): boolean {
    if (this.storage.get('role') === 'Admin') {
      return true;
    } else {
      return false;
    }
  }

}
