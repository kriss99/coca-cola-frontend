import { Component, OnInit, OnDestroy } from '@angular/core';
import { UserService } from 'src/app/core/services/user/user.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AppService } from 'src/app/core/services/app/app.service';

@Component({
  selector: 'app-help-desk',
  templateUrl: './help-desk.component.html',
  styleUrls: ['./help-desk.component.css']
})
export class HelpDeskComponent implements OnInit, OnDestroy {

  public editGroup: FormGroup;
  private subscription: Subscription;
  public currentTheme = 'default';

  constructor(
    private readonly userService: UserService,
    private readonly formBuilder: FormBuilder,
    private readonly appService: AppService,
  ) { }

  ngOnInit() {
    this.editGroup = this.formBuilder.group({
      subject: [
        '',
        [Validators.minLength(8), Validators.maxLength(25)]
      ],
      description: [
        '',
        [Validators.minLength(15), Validators.maxLength(100)]
      ]
    });
    this.subscription = this.appService.theme$.subscribe(
      res => {
        this.currentTheme = res;
      });
  }

  public sendEmail(): void {
    this.userService.sendEmailToHelpDesk(this.editGroup.value).subscribe(__ => __);
  }

  public getThemePrimaryClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: true, defaultAccent: false };
    } else {
      return { altPrimary: true, altAccent: false };
    }
  }

  public getThemeAccentClass() {
    if (this.currentTheme === 'default') {
      return { defaultPrimary: false, defaultAccent: true };
    } else {
      return { altPrimary: false, altAccent: true };
    }
  }

  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
