import { Component, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-create-customer',
  templateUrl: './create-customer.component.html',
  styleUrls: ['./create-customer.component.css']
})
export class CreateCustomerComponent implements OnInit {

  @Output()
  public customer = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  public createCustomer(name: string) {
    this.customer.emit({ name });
  }
}
