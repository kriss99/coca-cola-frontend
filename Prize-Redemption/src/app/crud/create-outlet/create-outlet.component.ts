import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Customer } from 'src/app/common/interfaces/customer';

@Component({
  selector: 'app-create-outlet',
  templateUrl: './create-outlet.component.html',
  styleUrls: ['./create-outlet.component.css']
})
export class CreateOutletComponent {

  @Output()
  public outlet = new EventEmitter<any>();

  @Input()
  public customers: Customer[];

  public createOutlet(name: string, customer): void {
    const customerID = this.customers.find(x => x.name === customer);
    const outlet = { name, customerId: customerID.id };
    this.outlet.emit(outlet);
  }
}
