import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Outlet } from 'src/app/common/interfaces/outlet';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  @Output()
  public user = new EventEmitter<any>();
  public editGroup: FormGroup;
  @Input()
  public outlets: Outlet[];
  @Input()
  public emails;
  @Input()
  public usernames;

  constructor(private readonly formBuilder: FormBuilder) { }

  ngOnInit() {
    this.editGroup = this.formBuilder.group({
      email: [
        // tslint:disable-next-line: max-line-length
        '', [Validators.minLength(5), Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]
      ],
      username: ['', [Validators.minLength(4), Validators.maxLength(20)]],
      password: ['', [Validators.pattern(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/)]]
    });
    this.usernames = this.usernames.map(x => x.username);
    this.emails = this.emails.map(x => x.email);
  }

  public createUser(outlet: string) {
    // tslint:disable-next-line: no-string-literal
    this.editGroup.value['outletId'] = outlet;
    this.user.emit(this.editGroup.value);
    this.editGroup.reset();
  }
}
