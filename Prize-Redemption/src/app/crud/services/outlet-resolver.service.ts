import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { catchError, tap } from 'rxjs/operators';
import { of } from 'rxjs';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { UserService } from 'src/app/core/services/user/user.service';
import { OutletService } from 'src/app/core/services/outlet/outlet.service';

@Injectable({
    providedIn: 'root'
})
export class OutletResolverService implements Resolve<any> {

    constructor(
        private readonly outletService: OutletService,
        private readonly notificator: NotificatorService,
    ) { }

    public resolve() {
        return this.outletService.getAllOutlets()
            .pipe(catchError(
                res => {
                    this.notificator.error(res.error.error);
                    return of({ outlets: [] });
                }
            ));
    }
}
