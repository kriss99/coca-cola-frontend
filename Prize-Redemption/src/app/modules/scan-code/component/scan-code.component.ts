import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NotificatorService } from 'src/app/core/services/notificator/notificator.service';
import { StorageService } from 'src/app/core/services/storage/storage.service';
import { BarcodeFormat } from '@zxing/library';
import { ScanCodeService } from 'src/app/core/services/scan-code/scan-code.service';
import { DialogService } from 'src/app/core/services/dialog/dialog.service';
import { Howl, Howler } from 'howler';
import { RedemptionRecord } from 'src/app/common/interfaces/redemption-record';
import { AppService } from 'src/app/core/services/app/app.service';
import { Subscription } from 'rxjs';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-scan-code',
  templateUrl: './scan-code.component.html',
  styleUrls: ['./scan-code.component.css']
})
export class ScanCodeComponent implements OnInit {


  constructor(
    private readonly notificator: NotificatorService,
    private readonly codeScanner: ScanCodeService,
    private readonly confirmDialog: DialogService,
    private readonly formBuilder: FormBuilder,
  ) {
    this.redemptionCodeForm = this.formBuilder.group({
      codeField: ['', Validators.required ]
   });
  }

  @ViewChild('audioOption', { static: true }) audioPlayerRef: ElementRef;

  public showScanningForPrizeItem = false;
  public codeToAddItemPrizeTo: string;
  public audio = new Audio('assets/audio/beep-06.mp3');

  redemptionCodeForm: FormGroup;

  formatsEnabled: BarcodeFormat[] = [
    BarcodeFormat.CODE_128,
    BarcodeFormat.QR_CODE,
  ];

  public onCodeResult(resultString: string) {
    this.audio.play();
    this.redemptionCodeForm.controls['codeField'].setValue(resultString) ;
  }

  public submitCode(inputCode: string) {

    if (this.showScanningForPrizeItem) {
      this.submitItemPrizeCode(this.codeToAddItemPrizeTo, inputCode);
    } else {
      this.codeScanner.checkCode(inputCode).subscribe(
        (res: any) => {
          if (res.id) {
            this.confirmDialog.openConfirmDialog(`This code is not yet redeemed! Would you like to redeem it?`)
              .afterClosed().subscribe(selectedYes => {
                if (selectedYes === true) {
                  this.redeemCode(inputCode);
                } else {
                  this.cancelRedeemption(inputCode);
                }
              });
          } else {
            this.declineRemeption(inputCode, res);
          }
        },
        error => {
          this.notificator.error(error.error.message);
        }
      );
    }
  }

  public submitItemPrizeCode(codeToAddItemPrizeTo: string, itemPrizeCode: string) {
    this.codeScanner.addPrizeItemToCode(codeToAddItemPrizeTo, itemPrizeCode).subscribe(
      response => {
        this.showScanningForPrizeItem = false;
        this.notificator.success(`Successfuly added prize item!`);
      },
      error => this.notificator.error(error.message),
    );
  }

  public redeemCode(codeToRedeem: string) {
    this.codeScanner.markCode(codeToRedeem, 'Redeemed').subscribe(
      response => {
        this.confirmDialog.openConfirmDialog(
          `You have successfully redeemed a ${(response as any).itemPrizeType} prize! Would you like to add a prize item code as well?`)
          .afterClosed().subscribe(selectedYes => {
            if (selectedYes === true) {
              this.showScanningForPrizeItem = true;
              this.codeToAddItemPrizeTo = codeToRedeem;
            }
          });
      },
      error => this.notificator.error(error.message),
    );
  }

  public cancelRedeemption(codeNotToRedeem: string) {
    this.codeScanner.markCode(codeNotToRedeem, 'Cancelled').subscribe(
      response => {
        this.notificator.success(`Redemption record marked as 'Canceled'!`);
      },
      error => this.notificator.error(error.message),
    );
  }

  public declineRemeption(codeToDecline: string, previousRedemptionAttempts: RedemptionRecord[]) {
    this.codeScanner.markCode(codeToDecline, 'Declined').subscribe(
      (resMarkCode: any) => {
        this.confirmDialog.openDeclinedRedemptionCodeDialog(previousRedemptionAttempts).afterClosed().subscribe(selectedYes => {
          if (selectedYes === true) {
            this.codeScanner.reportCode(codeToDecline).subscribe();
          }
        });
      });
  }

  ngOnInit() {
  }

}
