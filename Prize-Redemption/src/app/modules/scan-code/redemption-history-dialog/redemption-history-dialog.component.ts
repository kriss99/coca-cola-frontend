import { Component, OnInit, Inject, ViewChild, Input } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef, MatTableDataSource, MatSort, Sort, MatPaginator } from '@angular/material';
import { RedemptionRecord } from '../../../common/interfaces/redemption-record';

@Component({
  selector: 'app-redemption-history-dialog',
  templateUrl: './redemption-history-dialog.component.html',
  styleUrls: ['./redemption-history-dialog.component.css']
})
export class RedemptionHistoryDialogComponent implements OnInit {
  columnsToDisplay = ['timeStamp', 'outletName', 'customerName'];

  public records: RedemptionRecord[] = [];
  public dataSource: MatTableDataSource<RedemptionRecord>;

  @ViewChild(MatPaginator, {static: false}) paginator: MatPaginator;

  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<RedemptionHistoryDialogComponent>) {
      setTimeout(() => this.dataSource.paginator = this.paginator);
    }

    public getRedeemedOrDeclinedClass(status: string) {
      if (status === 'Redeemed') {
        return { redeemedRecord: true };
      } else {
        return { declinedRecord: true };
      }
    }

  ngOnInit() {
    this.records = this.data.redemptionRecordsList.sort((a, b) => {
      return new Date(a.timeStamp).getTime() - new Date(b.timeStamp).getTime();
    });
    this.dataSource = new MatTableDataSource<RedemptionRecord>(this.records);
  }

  closeDialog() {
    this.dialogRef.close(false);
  }
}
